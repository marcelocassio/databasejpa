package com.marcelo.repo.admin;

import org.springframework.data.jpa.repository.JpaRepository;

import com.marcelo.model.admin.Admin;


public interface IAdminRepo extends JpaRepository<Admin, Integer>{

}