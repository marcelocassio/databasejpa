package com.marcelo.repo.user;

import org.springframework.data.jpa.repository.JpaRepository;

import com.marcelo.model.user.User;

public interface IUserRepo extends JpaRepository<User, Integer> {

}
